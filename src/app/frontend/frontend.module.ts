import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { HomepageComponent } from './homepage/homepage.component';
import {ScrollToModule} from 'ng2-scroll-to';


export const WebRoutes = [
  {
    path: '',
    component: HomepageComponent,
  }
];

@NgModule({
  imports: [
    CommonModule,
    ScrollToModule.forRoot(),
    RouterModule.forChild(WebRoutes),
  ],
  declarations: [LoginComponent, HomepageComponent]
})
export class FrontendModule { }
