import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule } from '@angular/router'
import { adminLteConf } from './admin/admin-lte.conf';

import { AppComponent } from './app.component';

import { rootRoutes } from './app.routes';
import { LoadingPageModule, MaterialBarModule } from 'angular-loading-page';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    LoadingPageModule, MaterialBarModule,
    RouterModule.forRoot(rootRoutes, {
      // enableTracing :true, // For debugging
      preloadingStrategy: PreloadAllModules,
      initialNavigation: 'enabled',
      useHash: false
    }),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
