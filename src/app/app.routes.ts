import { Routes } from '@angular/router';


export const rootRoutes: Routes = [
  {
    path: '',
    loadChildren: 'app/frontend/frontend.module#FrontendModule'
  },
  {
    path: 'admin',
    loadChildren: 'app/admin/admin.module#AdminModule'
  }
];