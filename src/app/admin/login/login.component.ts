import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BackendApiService } from '../services/backend-api.service';
import { Router } from '@angular/router';
import { AlertService } from '../services/alert.service';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  register: FormGroup;
  isSubmitted: boolean = false;
  result: any = null;

  constructor(private frmBuilder: FormBuilder, private auth: BackendApiService, private alert: AlertService, private router: Router) { 
    
  }

  ngOnInit() {
    this.register = this.frmBuilder.group({
      email:["", [Validators.required]],
      password:["", [Validators.required]],
    });
  }

  get email() { return this.register.get('email'); }
  get password() { return this.register.get('password'); }

  save(){
    debugger;
    console.log(this.register.value);
    this.isSubmitted = true;
    if(!this.register.valid){
      return;
    } else {
      // Code to save the data
      // userService.Save(this.register.value);
      this.auth.postData(this.register.value)
      .then(data => {
        console.log(data);
        if(data) {
          this.alert.AlertSuccess();
          this.router.navigate(['/admin']);
        } else {
          this.alert.AlertError();
        }
        // this.reset();
        //toast.present();
      },err => {
        console.log(err);
      });
      // this.result = this.register.value;
      // setTimeout(()=> {
      // this.result = null;
      // this.reset();
      // }, 2000);
    }
        
   
  }
  
  reset(){
    this.isSubmitted = false;
    this.register.reset();
  }
}
