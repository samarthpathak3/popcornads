import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import  {Http, Headers, RequestOptions, HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { LoginComponent } from './login/login.component';
import { adminLteConf } from './admin-lte.conf';
import { CoreModule } from './core/core.module';
import { LayoutModule } from 'angular-admin-lte';
import { LoadingPageModule, MaterialBarModule } from 'angular-loading-page';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AdminComponent } from './admin.component';
import { BackendApiService } from './services/backend-api.service';
import { AlertService } from './services/alert.service';

export const AdminRoutes = [
  {
    path: '',
    component: AdminComponent
  },
  {
    path: 'login',
    component: LoginComponent,
    data: {
      customLayout: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    CoreModule,
    HttpClientModule,
    HttpModule,
    LayoutModule.forRoot(adminLteConf),
    LoadingPageModule, MaterialBarModule,
    RouterModule.forChild(AdminRoutes),
    FormsModule, ReactiveFormsModule
  ],
  declarations: [AdminComponent, LoginComponent],
  bootstrap: [AdminComponent],
  providers: [BackendApiService, AlertService]
})
export class AdminModule { }
