import { Component, OnInit } from '@angular/core';
import swal from 'sweetalert2'; 
/*
  Generated class for the AuthServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/

export class AlertService {

  constructor() {
    console.log('Hello AlertService Provider');
  }

  AlertSuccess(){
    swal({
        title: 'Success!',
        text: 'Do you want to continue',
        type: 'success',
        confirmButtonText: 'Cool'
      })
  }

  AlertError() {
    swal({
        title: 'Error!',
        text: 'Do you want to continue',
        type: 'error',
        confirmButtonText: 'Cool'
      })
  }

  ngOnInit() {
   
  }

}
