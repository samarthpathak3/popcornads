import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

/*
  Generated class for the AuthServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class BackendApiService {

  constructor(public http: HttpClient, private httpheader: Http) {
    console.log('Hello AuthServiceProvider Provider');
  }

  public getData(){ 
    return new Promise(resolve => {
      this.http.get("").subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
       
      });
    });    
  }


  public postData( Data){
    return new Promise(resolve => {
      this.http.post("http://localhost:3000/api/v1/users/checkUser", Data)
      .subscribe(
        res => {
          resolve(res);
        },
        err => {
          resolve(err);
        }
      );
    });
  }

}
